# Searx Nord Solid css style
**To work properly install simple dark theme in searx preferences**

<img src="/Images/main.png" alt="Main" width="80%"/>

<img src="/Images/preferences.png" alt="Preferences" width="80%"/>

<img src="/Images/search.png" alt="Search Example" width="80%"/>

**All images are available [here](/Images/)**


## If you find any issue, please, report it [here](https://gitlab.com/57d/searx-nord-solid/-/issues/new)
